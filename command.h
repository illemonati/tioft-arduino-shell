
#ifndef COMMAND_H_
#define COMMAND_H_

#include "invoked_command.h"

typedef void (*CommandFunction)(InvokedCommand& invoked_command);

typedef struct Command {
    const char* name;
    const char* description;
    CommandFunction command_function;
} Command;

#endif