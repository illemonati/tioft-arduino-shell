

#ifndef COMMANDS_H_
#define COMMANDS_H_
#include "command.h"
#include "commands/help-command.h"
#include "commands/version-command.h"
#include "generate_help_command.h"

class Commands {
   public:
    Command* commands;
    Command* get_command(const char* command_name) {
        for (size_t i = 0; i < sizeof(commands); i++) {
            if (strcmp(command_name, this->commands[i].name) == 0) {
                return &commands[i];
            }
        }
        return nullptr;
    }
    Commands(Command* commands) : commands(commands) {
        CommandFunction help_command_function = generate_help_command_function(commands);
        this->commands[0].command_function = help_command_function;
    }
};

#endif