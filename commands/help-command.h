#ifndef HELP_COMMAND_H_
#define HELP_COMMAND_H_

#include "../command.h"

const Command help_command{
    .name = "help",
    .description = "gets help for commands",
    .command_function = nullptr};

#endif