

#ifndef VERSION_COMMAND_H_
#define VERSION_COMMAND_H_

#include "../command.h"
#include "../invoked_command.h"

constexpr char version[] = "0.0.1";
constexpr char name[] = "tioft-arduino-shell";

const char* full_version_string() {
    size_t version_len = strlen(version) + strlen(name) + 10;
    char* res = (char*)malloc(version_len);
    sprintf(res, "%s version-%s", name, version);
    return res;
}

void version_function(InvokedCommand& invoked_command) {
    Serial.println(full_version_string());
}

const Command version_command = {
    .name = "version",
    .description = "get current version",
    .command_function = version_function};

#endif