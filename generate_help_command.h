#ifndef GENERATE_HELP_COMMAND_H
#define GENERATE_HELP_COMMAND_H

#include "command.h"
#include "commands.h"
#include "utils.h"

CommandFunction generate_help_command_function(Command commands[]) {
    char* total_help_string;
    sprintf(total_help_string, "help menu - %d commands total", sizeof(commands));
    size_t max_command_name_length = 0;
    for (size_t i = 0; i < sizeof(commands); i++) {
        size_t command_name_len = strlen(commands[i].name);
        if (command_name_len > max_command_name_length) {
            max_command_name_length = max_command_name_length;
        }
    }
    size_t total_name_length = max_command_name_length + 2;
    for (size_t i = 0; i < sizeof(commands); i++) {
        char* display_name_str;
        sprintf(display_name_str, "%s%s", commands[i].name, generate_padding(total_name_length - strlen(commands[i].name), " "));
        sprintf(total_help_string, "%s\n%s-  %s", total_help_string, commands[i].name, commands[i].description);
    }
    auto help_command_function = [](InvokedCommand& invoked_command) {
                Serial.println(total_help_string);
    };
    return help_command_function;
}

#endif