
#ifndef INVOKED_COMMAND_H_
#define INVOKED_COMMAND_H_

#define MAX_INVOKED_COMMAND_ARGUMENTS 8

class InvokedCommand {
   public:
    const char* raw_message;
    size_t n_arguments = 0;
    char** arguments = nullptr;
    InvokedCommand(const char* raw_message) : raw_message(raw_message) {
        this->parse_arguments();
    }
    ~InvokedCommand() {
        free(arguments);
    }
    char* get_command_name() {
        if (this->n_arguments < 1) return nullptr;
        return this->arguments[0];
    }

   private:
    void parse_arguments() {
        this->n_arguments = 0;
        this->arguments = (char**)calloc(MAX_INVOKED_COMMAND_ARGUMENTS, sizeof(char*));
        for (char* argument = strtok((char*)raw_message, " "); argument != NULL; argument = strtok(NULL, " ")) {
            this->arguments[this->n_arguments] = argument;
            this->n_arguments++;
            if (this->n_arguments >= MAX_INVOKED_COMMAND_ARGUMENTS) break;
        }
    }
};

#endif