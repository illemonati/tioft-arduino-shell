

#include "command.h"
#include "commands.h"
#include "invoked_command.h"

Command _commands[] = {
    //always have this at index 0
    help_command,
    version_command,
};

const Commands commands(_commands);

void setup() {
    // initialize digital pin LED_BUILTIN as an output.
    pinMode(LED_BUILTIN, OUTPUT);
    Serial.begin(9600);
}

// the loop function runs over and over again forever
void loop() {
    while (Serial.available() > 0) {
        String str = Serial.readStringUntil('\n');
        Serial.println(str);
        handle_command(str);
        Serial.flush();
    }
}

void handle_command(String raw_command) {
    Serial.println("> " + raw_command);
    const char* raw_command_cstr = raw_command.c_str();
    InvokedCommand invoked_command(raw_command_cstr);
    char* command_name;
    command_name = invoked_command.get_command_name();
    if (command_name == nullptr) return;

    Command* command = commands.get_command(command_name);
    if (command == nullptr) {
        const char* beginning = "Unknown command: ";
        Serial.print(beginning);
        Serial.println(command_name);
        return;
    }
    if (command->command_function != nullptr) {
        command->command_function(invoked_command);
    }
}
