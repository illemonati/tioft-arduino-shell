
#ifndef UTILS_H_
#define UTILS_H_

char* generate_padding(char padding_char, size_t n_padding) {
    char* res = (char*)malloc(n_padding * sizeof(char));
    for (size_t i = 0; i < n_padding; i++) {
        res[i] = padding_char;
    }
    return res;
}

#endif